var files_dup =
[
    [ "BNO055.py", "BNO055_8py.html", "BNO055_8py" ],
    [ "closedLoop.py", "closedLoop_8py.html", [
      [ "closedLoop.ClosedLoop", "classclosedLoop_1_1ClosedLoop.html", "classclosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "DRV8847.py", "DRV8847_8py.html", "DRV8847_8py" ],
    [ "encoder.py", "encoder_8py.html", [
      [ "encoder.Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "main.py", "main_8py.html", null ],
    [ "mainpage.py", "mainpage_8py.html", null ],
    [ "motor_driver.py", "motor__driver_8py.html", "motor__driver_8py" ],
    [ "reportpage.py", "reportpage_8py.html", null ],
    [ "shares.py", "shares_8py.html", [
      [ "shares.Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "shares.Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ]
    ] ],
    [ "task_controller.py", "task__controller_8py.html", "task__controller_8py" ],
    [ "task_data_collect.py", "task__data__collect_8py.html", [
      [ "task_data_collect.Task_Data_Collect", "classtask__data__collect_1_1Task__Data__Collect.html", "classtask__data__collect_1_1Task__Data__Collect" ]
    ] ],
    [ "task_encoder.py", "task__encoder_8py.html", "task__encoder_8py" ],
    [ "task_imu.py", "task__imu_8py.html", "task__imu_8py" ],
    [ "Task_Motor.py", "Task__Motor_8py.html", [
      [ "Task_Motor.Task_Motor", "classTask__Motor_1_1Task__Motor.html", "classTask__Motor_1_1Task__Motor" ]
    ] ],
    [ "task_panel.py", "task__panel_8py.html", [
      [ "task_panel.Task_Panel", "classtask__panel_1_1Task__Panel.html", "classtask__panel_1_1Task__Panel" ]
    ] ],
    [ "touchpanel.py", "touchpanel_8py.html", "touchpanel_8py" ],
    [ "tuningpage.py", "tuningpage_8py.html", null ]
];