var annotated_dup =
[
    [ "BNO055", null, [
      [ "BNO055", "classBNO055_1_1BNO055.html", "classBNO055_1_1BNO055" ]
    ] ],
    [ "closedLoop", null, [
      [ "ClosedLoop", "classclosedLoop_1_1ClosedLoop.html", "classclosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "DRV8847", null, [
      [ "DRV8847", "classDRV8847_1_1DRV8847.html", "classDRV8847_1_1DRV8847" ],
      [ "Motor", "classDRV8847_1_1Motor.html", "classDRV8847_1_1Motor" ]
    ] ],
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "motor_driver", null, [
      [ "DRV8847", "classmotor__driver_1_1DRV8847.html", "classmotor__driver_1_1DRV8847" ],
      [ "Motor", "classmotor__driver_1_1Motor.html", "classmotor__driver_1_1Motor" ]
    ] ],
    [ "shares", null, [
      [ "Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ],
      [ "Share", "classshares_1_1Share.html", "classshares_1_1Share" ]
    ] ],
    [ "task_controller", null, [
      [ "Task_Controller", "classtask__controller_1_1Task__Controller.html", "classtask__controller_1_1Task__Controller" ]
    ] ],
    [ "task_data_collect", null, [
      [ "Task_Data_Collect", "classtask__data__collect_1_1Task__Data__Collect.html", "classtask__data__collect_1_1Task__Data__Collect" ]
    ] ],
    [ "task_data_collect_complicated", null, [
      [ "Task_Data_Collect", "classtask__data__collect__complicated_1_1Task__Data__Collect.html", "classtask__data__collect__complicated_1_1Task__Data__Collect" ]
    ] ],
    [ "task_encoder", null, [
      [ "Task_Encoder", "classtask__encoder_1_1Task__Encoder.html", "classtask__encoder_1_1Task__Encoder" ]
    ] ],
    [ "task_imu", null, [
      [ "Task_IMU", "classtask__imu_1_1Task__IMU.html", "classtask__imu_1_1Task__IMU" ]
    ] ],
    [ "Task_Motor", null, [
      [ "Task_Motor", "classTask__Motor_1_1Task__Motor.html", "classTask__Motor_1_1Task__Motor" ]
    ] ],
    [ "task_panel", null, [
      [ "Task_Panel", "classtask__panel_1_1Task__Panel.html", "classtask__panel_1_1Task__Panel" ]
    ] ],
    [ "Task_User", null, [
      [ "Task_User", "classTask__User_1_1Task__User.html", "classTask__User_1_1Task__User" ]
    ] ],
    [ "touchpanel", null, [
      [ "TouchPanel", "classtouchpanel_1_1TouchPanel.html", "classtouchpanel_1_1TouchPanel" ]
    ] ]
];